import sys
from functools import partial

import matplotlib.pyplot as plt
import numpy as np
from pennylane_synqs import Ut, Upt, HartreeFock
from pennylane_synqs import (
    decompose_Upt,
    decompose_Ut_into_ZXZXZ,
    ensure_positive_Uint_params,
    merge_sequentual_Ut,
)
from pennylane_synqs.synqs_transforms import per_gate_gaussian_noise, ensure_positive_lattice_params
from tqdm import tqdm
import re


def ansatz(params, wires, nalpha, nbeta, depth):
    """QNP fabric ansatz
    """
    HartreeFock(nalpha, nbeta, wires=wires)
    # start with an "odd" layer to not have any useless gates
    # if an even number of Upt gates fits on the wires
    # because we do not do backtracking
    if (len(wires) // 4) % 2 == 0:
        curr_layer = 1
        depth += 1
    else:
        curr_layer = 0
    pivot = 2
    param_num = 0
    while curr_layer < depth:
        if curr_layer % 2 == 0:
            for start in range(0,len(wires),4):
                stop=start+4
                sub_wires=[wire for wire in range(start, stop)]
                Upt(params[param_num], np.pi / 2, wires=sub_wires)
                Ut(params[param_num + 1], np.pi / 2, 0, wires=[sub_wires[0], sub_wires[2]])
                Ut(params[param_num + 1], np.pi / 2, 0, wires=[sub_wires[1], sub_wires[3]])
                param_num += 2
        elif curr_layer % 2 == 1:
            for start in range(pivot,len(wires),4):
                stop=start+4
                if stop>len(wires):
                    continue
                sub_wires=[wire for wire in range(start, stop)]
                Upt(params[param_num], np.pi / 2, wires=sub_wires)
                Ut(params[param_num + 1], np.pi / 2, 0, wires=[sub_wires[0], sub_wires[2]])
                Ut(params[param_num + 1], np.pi / 2, 0, wires=[sub_wires[1], sub_wires[3]])
                param_num += 2

        curr_layer+=1

noise_level = {
    'LatticeRX': 0,
    'LatticeRZ': 0,
    'Uint': 0,
    'LatticeRX Slope': 0,
    'Uint Slope': 0,
}

compiled_circuit = None

def error_benchmark(infile = None, error_levels=None, number_of_repetitions_for_estimation=10, gates=None):
    import os

    from pennylane import numpy as np
    import pennylane as qml
    from pennylane_synqs import Ut, Upt, HartreeFock

    try:
        if infile is None:
            infile = sys.argv[1]
        npzfile = np.load(infile, allow_pickle=True)
    except IndexError:
        print(f"Usage for MPQ people: python {__file__.split('/')[-1]} path_to_data_we_sent_you.npz")
        quit(0)

    nact=int(npzfile["nact"]) # number of spatial orbitals, number of spin orbitals is 2 * nact
    nalpha=int(npzfile["nalpha"]) # number of up electrons
    nbeta=int(npzfile["nbeta"]) # number of down electrons
    depth=int(npzfile["depth"]) # depth of the VQE ansatz

    # you can use both of these devices:
    # dev = qml.device("default.qubit", wires=2 * nact)
    dev = qml.device("synqs.fqe", wires=2 * nact, nalpha=nalpha, nbeta=nbeta)
    with_shot_noise = False

    decomposition = npzfile["decomposition"].item() # need to use .item() here because npz wrapps the dictionary in a one element numpy array
    optimal_params = npzfile["optimal_params"]

    # now we can loop over the leafs:
    def compute_df_energy(params, disp=True):
        energy = npzfile["offset"] # contains the core energy and contributions from the double factorization

        for t, (givens_angles, observables, coeffs, shots) in enumerate(zip(decomposition["givens_angles"], decomposition["observables"], decomposition["coeffs"], npzfile["shot_distribution"])):


            if with_shot_noise:
                print(f'Number of shots = {shots}')
                dev.shots = int(shots)

            @qml.qnode(dev, diff_method="best", cache=None, expansion_strategy="device")
            def qnode(params):
                wires=range(2 * nact)
                ansatz(params, wires=wires, nalpha=nalpha, nbeta=nbeta, depth=depth)
                # givens circuit for double factorization
                if len(givens_angles) == 1:
                    Ut(givens_angles[0], np.pi / 2, 0, wires=[0, 2])
                    Ut(givens_angles[0], np.pi / 2, 0, wires=[1, 3])
                else:
                    gate_num = 0
                    for current_layer in range(len(wires) // 2):
                        if current_layer % 2 == 0:
                            for start in range(0, len(wires) - 4 + 1, 2 * 2):
                                Ut(givens_angles[gate_num], np.pi / 2, 0, wires=[start, start + 2])
                                Ut(givens_angles[gate_num], np.pi / 2, 0, wires=[start + 1, start + 3])
                                gate_num += 1
                        elif current_layer % 2 == 1:
                            for start in range(2, len(wires) - 4 + 1, 2 * 2):
                                Ut(givens_angles[gate_num], np.pi / 2, 0, wires=[start, start + 2])
                                Ut(givens_angles[gate_num], np.pi / 2, 0, wires=[start + 1, start + 3])
                                gate_num += 1

                return [qml.expval(qml.PauliZ(observable[0][0]) if len(observable) == 1 else qml.PauliZ(observable[0][0]) @ qml.PauliZ(observable[1][0])) for observable in observables]

            qnode_compiled = partial(qml.compile,
                pipeline=[
                    decompose_Upt,
                    merge_sequentual_Ut,
                    decompose_Ut_into_ZXZXZ,
                    ensure_positive_Uint_params,
                    ensure_positive_lattice_params,
                    partial(per_gate_gaussian_noise, noise_strength=noise_level),
                ],
                basis_set=["HartreeFock", "Uint", "LatticeRX", "LatticeRZ"],
                expand_depth=0,
            )(qnode)

            global compiled_circuit
            compiled_circuit = qnode_compiled

            if disp:
                print(f"Circuit for leaf {t} to be run {shots} times:")
                print(qml.draw(qnode_compiled, wire_order=range(4), expansion_strategy="device")(optimal_params))
                # print("Number of operations:")
                # print(np.size(qnode_compiled.tape.operations))
                print("With operations:")
                print(qnode_compiled.tape.operations)
                if t == 0:
                    # save a picture of the circuit for leaf 0:
                    fig, ax = qml.draw_mpl(qnode_compiled, wire_order=list(range(0, 2 * nact, 2)) + list(range(1, 2 * nact, 2)), style='pennylane')(optimal_params)
                    from matplotlib.backends.backend_pdf import PdfPages
                    with PdfPages(infile[0:-4] + "_compiled_circuit.pdf") as pdf:
                        pdf.savefig(fig)

                    fig, ax = qml.draw_mpl(qnode, wire_order=range(2 * nact), style='pennylane')(optimal_params)
                    with PdfPages(infile[0:-4] + "_circuit.pdf") as pdf:
                        pdf.savefig(fig)

                ### Hot fix to get histogram of the gate parameters

                output_string = qml.draw(qnode_compiled, wire_order=range(4), expansion_strategy="device")(optimal_params)

                lattice_rx_parameters = re.findall(r'LatticeRX\((-?\d+\.\d+)\)', output_string)
                lattice_rx_parameters = [float(num) for num in lattice_rx_parameters]

                lattice_rz_parameters = re.findall(r'LatticeRZ\((-?\d+\.\d+)\)', output_string)
                lattice_rz_parameters = [float(num) for num in lattice_rz_parameters]

                uint_parameters = re.findall(r'Uint\((-?\d+\.\d+)\)', output_string)
                uint_parameters = [float(num) for num in uint_parameters]

                plt.figure()
                plt.hist(lattice_rz_parameters, label='LatticeRZ')
                plt.hist(lattice_rx_parameters, label='LatticeRX')
                plt.hist(uint_parameters, label='Uint')
                plt.legend()
                plt.savefig('gate_parameters_histogram.png')

                print(lattice_rx_parameters, lattice_rz_parameters, uint_parameters)


            results = qnode_compiled(params)
            energy += np.dot(results, np.array(coeffs))

        return energy


    for gate in gates:
        print(f'=== Gate {gate} ===')
        print()
        energy_stddev = np.array([])
        energy_error = np.array([])
        energy_estimate = np.array([])
        energy_offset = np.array([])

        all_energy_estimates = np.array([])


        noise_level = {
            'LatticeRX': 0,
            'LatticeRX Offset': 0,
            'LatticeRZ': 0,
            'LatticeRZ Offset': 0,
            'Uint': 0,
            'Uint Offset': 0,
            'LatticeRX Slope': 0,
            'Uint Slope': 0,
        }
        for error_level in tqdm(error_levels):
            energies = np.array([])
            if gate in ['LatticeRX Offset', 'LatticeRZ Offset', 'Uint Offset']:
                # Draw offset according to error level for full energy calculation
                for _ in tqdm(range(number_of_repetitions_for_estimation)):
                    noise_level[gate] = np.random.normal(loc=0.0, scale=error_level)
                    energy = compute_df_energy(optimal_params, disp=False)
                    energies = np.append(energies, energy)
            else:
                # Set noise level and compute energy
                noise_level[gate] = float(error_level)
                for _ in tqdm(range(number_of_repetitions_for_estimation)):
                    energy = compute_df_energy(optimal_params, disp=False)
                    energies = np.append(energies, energy)


            energy_stddev = np.append(energy_stddev, np.std(energies))
            energy_error = np.append(energy_error, np.abs(npzfile['vqe_energy'] - np.mean(energies)))
            energy_estimate = np.append(energy_estimate, np.mean(energies))
            energy_offset = np.append(energy_offset, np.abs(npzfile['vqe_energy'] - np.mean(energies)))
            all_energy_estimates = np.append(all_energy_estimates, energies)

        # print(compute_df_energy(optimal_params, disp=True))

        outfile = f'{infile[:-4]}_{gate}.npz'

        np.savez(outfile,
                 all_energy_calculations=all_energy_estimates,
                 error_level=error_levels,
                 energy_stdev=energy_stddev,
                 energy_error=energy_error,
                 energy_offset=energy_offset,
                 energy_estimate=energy_estimate,
                 number_of_operations=np.size(compiled_circuit.tape.operations),
                 number_of_repetitions_for_estimation=number_of_repetitions_for_estimation,
                 )

    # # obviously you can also compute the DF VQE energy also at other params
    # print("energy at random parameters:")
    # print(compute_df_energy(np.random.randn(*optimal_params.shape), disp=False))
    # and you can also use compute_df_energy() to optimize a VQE, even with gradients,
    # at least for with_shot_noise=False and when not using the merge Ut transform:
    # print("jacobian at random parameters:")
    # print(qml.jacobian(compute_df_energy)(np.random.randn(*optimal_params.shape), disp=False))


if __name__ == "__main__":

    ###########################################
    ### Parameters for full error benchmark ###
    ###########################################
    error_levels = 10 ** np.linspace(-4.5, -1.5, 7) # np.linspace(-6, -1, 7)  # 7
    number_of_repetitions_for_estimation = 150  # 20  # 25

    gates_to_compute = ['LatticeRZ', 'Uint']  # 'LatticeRX', 'LatticeRZ', 'Uint', 'LatticeRX Offset', 'LatticeRZ Offset', 'Uint Offset'

    ################################
    ### Parameters for histogram ###
    ################################
    # error_levels = [10 ** -3]  # np.linspace(-6, -1, 7)  # 7
    # number_of_repetitions_for_estimation = 150  # 20  # 25
    #
    # gates_to_compute = ['LatticeRX', 'LatticeRZ', 'Uint', 'LatticeRX Offset', 'LatticeRZ Offset', 'Uint Offset']

    for file in ['h4_tetrahedral.npz']:
        print('==================')
        print(f'Calculating {file}')
        print()
        error_benchmark(infile=file, error_levels=error_levels, number_of_repetitions_for_estimation=number_of_repetitions_for_estimation, gates=gates_to_compute)



