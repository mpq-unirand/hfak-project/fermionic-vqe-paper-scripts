import pennylane as qml

from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from scipy.sparse import csr_matrix

plt.close('all')

mpl.rcParams['font.family'] = 'Arial'
mpl.rcParams['font.size'] = 16


## This script plots the data for the tetrahedral H4 circuit shown in the paper. Data provided by Email from Fotios on May 21st 2024 in two .npy files.

#############
# 1 Inspect and plot the normalized Hamiltonian in some optimized basis
hamiltonian = np.load('mpq_fermionic_hamiltonian_matrix_array.npy')
normed_hamiltonian = np.abs(hamiltonian)/np.max(np.abs(hamiltonian))
plt.figure(11)
plt.imshow(np.abs(hamiltonian), cmap='YlGnBu', norm=colors.LogNorm(vmin=1e-4, vmax=1),origin='lower')
plt.colorbar()
# plt.title('Sparse Matrix Heatmap')
plt.savefig('hamiltonian_h4_tetrahedral_20240522_fotios.pdf')
plt.show()


############################
# 2 Find and plot the ground state of the matrix
# Find the smallest eigenvalue and the corresponding eigenvector
eigenvalues, eigenvectors = np.linalg.eigh(hamiltonian)

# Smallest eigenvalue
smallest_eigenvalue = eigenvalues[0]

# Corresponding eigenvector
corresponding_eigenvector = eigenvectors[:, 0]
print('matrix ground state')
print(np.sort(corresponding_eigenvector))
plt.figure(13)
# Create a bar plot
plt.bar(range(len(corresponding_eigenvector)), np.abs(corresponding_eigenvector))

# Hide the right and top spines
plt.gca().spines['right'].set_visible(False)
plt.gca().spines['top'].set_visible(False)

# Set y-axis to log scale
plt.yscale('log')
plt.ylim([10**-3,1])
plt.xlim( [-1, 36]) 

# Set aspect ratio 
plt.gca().set_aspect(3.5, adjustable='box')

#plt.savefig("smallest_eigenvector_h4tetrahedral_20240522_fotios.png", dpi=450)
plt.savefig("smallest_eigenvector_h4tetrahedral_20240522_fotios.pdf")

# Show the plot
plt.show()




##############################
# Now look at the CASCI ground state vector provided by Covestro
# Unfortunately the order of the basis states is different to the Hamiltonian matrix and we do not know the basis reordering 
# We see that the CASCI state and the ground state of the Hamiltonian matrix are the same, just in a different order. 

# Load dictionary from the .npy file
loaded_dict = np.load('mpq_casci_state.npy', allow_pickle=True).item()


# Extract values as a NumPy array
values_list = list(loaded_dict.values())
casci_groundstate = np.array(values_list)
print('casci ground state')
print(np.sort(casci_groundstate))
plt.figure(13)
# Create a bar plot
plt.bar(range(len(casci_groundstate)), np.abs(casci_groundstate))

# Hide the right and top spines
plt.gca().spines['right'].set_visible(False)
plt.gca().spines['top'].set_visible(False)

# Set y-axis to log scale
plt.yscale('log')
plt.ylim([10**-3,1])
plt.xlim( [-1, 36]) 

# Set aspect ratio 
plt.gca().set_aspect(3.5, adjustable='box')

#plt.savefig("casci_h4_tetrahedral_20240522_fotios.pdf")
plt.savefig("casci_h4_tetrahedral_20240522_fotios.eps")
#plt.savefig("casci_h4_tetrahedral_20240522_fotios.png", dpi=450)


# Show the plot
plt.show()