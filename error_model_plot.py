import numpy as np
from matplotlib import pyplot as plt
import scipy.optimize
import scipy.stats as stats


def myExpFunc(x, a, b):
    return a * x + b


UINT_COLOR = '#f4ad4b'
LRX_COLOR = '#6dc4ba'
LRZ_COLOR = '#367479'

x_values = np.linspace(-4.8, -1.2, 100)

plt.style.use('unirand')

SMALL_SIZE = 6
MEDIUM_SIZE = 8
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

fig = plt.figure(figsize=(3.3, 2.5))
ax = fig.add_subplot(111)
plt.xscale('log')
ax.set_yscale('log')
plt.xlabel('Relative perturbation')
plt.ylabel('RMS error (Ha)')

ax.axhline(1e-3, color='black', linestyle='dotted', label='Chemical precision')
# ax.text(2e-4, 8e-4, 'Chemical precision', verticalalignment='bottom', horizontalalignment='right')

##########################
### Load molecule data ###
##########################
molecule = 'h4_tetrahedral2'
number_of_energies_per_error_rate = 10
number_of_error_rates = 5

npzfile = np.load(f'data/{molecule}.npz')
vqe_energy = npzfile['vqe_energy']

################
### Plot LRX ###
################
outfile = f'data/{molecule}_LatticeRX.npz'
npzfile = np.load(outfile)

number_of_energies_per_error_rate = npzfile['number_of_repetitions_for_estimation']
number_of_error_rates = int(np.size(npzfile['all_energy_calculations']) / number_of_energies_per_error_rate)

energies = npzfile['all_energy_calculations']
energies = np.reshape(energies, (number_of_error_rates, number_of_energies_per_error_rate))
errors = np.sqrt((np.square(energies - vqe_energy)))
standard_error = np.std(errors, axis=1)
error = np.sqrt((np.square(energies - vqe_energy)).mean(axis=1))
ax.errorbar(npzfile['error_level'], error, yerr=standard_error, fmt='None', capsize=0, capthick=2, color=LRX_COLOR)
ax.scatter(npzfile['error_level'], error, label=r'$LRX$', s=10, color=LRX_COLOR)
params, cv = scipy.optimize.curve_fit(myExpFunc, np.log10(npzfile['error_level']), np.log10(error))
ax.plot(10 ** (x_values), 10 ** (myExpFunc(x_values, params[0], params[1])), linestyle='dashed', color=LRX_COLOR)

################
### Plot LRZ ###
################
outfile = f'data/{molecule}_LatticeRZ.npz'
npzfile = np.load(outfile)
number_of_energies_per_error_rate = npzfile['number_of_repetitions_for_estimation']
number_of_error_rates = int(np.size(npzfile['all_energy_calculations']) / number_of_energies_per_error_rate)
energies = npzfile['all_energy_calculations']
energies = np.reshape(energies, (number_of_error_rates, number_of_energies_per_error_rate))
errors = np.sqrt((np.square(energies - vqe_energy)))
standard_error = np.std(errors, axis=1)
error = np.sqrt((np.square(energies - vqe_energy)).mean(axis=1))
ax.errorbar(npzfile['error_level'], error, yerr=standard_error, fmt='None', capsize=0, capthick=2, color=LRZ_COLOR)
ax.scatter(npzfile['error_level'], error, label=r'$LRZ$', s=10, color=LRZ_COLOR)
params, cv = scipy.optimize.curve_fit(myExpFunc, np.log10(npzfile['error_level']), np.log10(error))
ax.plot(10 ** (x_values), 10 ** (myExpFunc(x_values, params[0], params[1])), linestyle='dashed', color=LRZ_COLOR)

#################
### Plot UINT ###
#################
outfile = f'data/{molecule}_Uint.npz'
npzfile = np.load(outfile)
number_of_energies_per_error_rate = npzfile['number_of_repetitions_for_estimation']
number_of_error_rates = int(np.size(npzfile['all_energy_calculations']) / number_of_energies_per_error_rate)
energies = npzfile['all_energy_calculations']
energies = np.reshape(energies, (number_of_error_rates, number_of_energies_per_error_rate))
errors = np.sqrt((np.square(energies - vqe_energy)))
standard_error = np.std(errors, axis=1)
error = np.sqrt((np.square(energies - vqe_energy)).mean(axis=1))
ax.errorbar(npzfile['error_level'], error, yerr=standard_error, fmt='None', capsize=0, capthick=2, color=UINT_COLOR,
            zorder=1)
ax.scatter(npzfile['error_level'], error, label=r'$U^{(int)}$', s=10, zorder=2, color=UINT_COLOR)
params, cv = scipy.optimize.curve_fit(myExpFunc, np.log10(npzfile['error_level']), np.log10(error))
ax.plot(10 ** (x_values), 10 ** (myExpFunc(x_values, params[0], params[1])), linestyle='dashed', color=UINT_COLOR)

#######################
### Plot LRX Offset ###
#######################
outfile = f'data/{molecule}_LatticeRX Offset.npz'
npzfile = np.load(outfile)
number_of_energies_per_error_rate = npzfile['number_of_repetitions_for_estimation']
number_of_error_rates = int(np.size(npzfile['all_energy_calculations']) / number_of_energies_per_error_rate)
energies = npzfile['all_energy_calculations']
energies = np.reshape(energies, (number_of_error_rates, number_of_energies_per_error_rate))
errors = np.sqrt((np.square(energies - vqe_energy)))
standard_error = np.std(errors, axis=1)
error = np.sqrt((np.square(energies - vqe_energy)).mean(axis=1))
ax.errorbar(npzfile['error_level'], error, yerr=standard_error, fmt='None', capsize=0, capthick=2, color=LRX_COLOR,
            zorder=3)
ax.scatter(npzfile['error_level'], error, label=r'$LRX$ static', s=10, color=LRX_COLOR, zorder=4, marker='v')
params, cv = scipy.optimize.curve_fit(myExpFunc, np.log10(npzfile['error_level']), np.log10(error))
ax.plot(10 ** (x_values), 10 ** (myExpFunc(x_values, params[0], params[1])), linestyle='dotted', color=LRX_COLOR)

#######################
### Plot LRZ Offset ###
#######################
outfile = f'data/{molecule}_LatticeRZ Offset.npz'
npzfile = np.load(outfile)
number_of_energies_per_error_rate = npzfile['number_of_repetitions_for_estimation']
number_of_error_rates = int(np.size(npzfile['all_energy_calculations']) / number_of_energies_per_error_rate)
energies = npzfile['all_energy_calculations']
energies = np.reshape(energies, (number_of_error_rates, number_of_energies_per_error_rate))
errors = np.sqrt((np.square(energies - vqe_energy)))
standard_error = np.std(errors, axis=1)
error = np.sqrt((np.square(energies - vqe_energy)).mean(axis=1))
ax.errorbar(npzfile['error_level'], error, yerr=standard_error, fmt='None', capsize=0, capthick=2, color=LRZ_COLOR,
            zorder=5)
ax.scatter(npzfile['error_level'], error, label=r'$LRZ$ static', s=10, color=LRZ_COLOR, zorder=6, marker='v')
params, cv = scipy.optimize.curve_fit(myExpFunc, np.log10(npzfile['error_level']), np.log10(error))
ax.plot(10 ** (x_values), 10 ** (myExpFunc(x_values, params[0], params[1])), linestyle='dotted', color=LRZ_COLOR)

########################
### Plot UINT Offset ###
########################
outfile = f'data/{molecule}_Uint Offset.npz'
npzfile = np.load(outfile)
number_of_energies_per_error_rate = npzfile['number_of_repetitions_for_estimation']
number_of_error_rates = int(np.size(npzfile['all_energy_calculations']) / number_of_energies_per_error_rate)
energies = npzfile['all_energy_calculations']
energies = np.reshape(energies, (number_of_error_rates, number_of_energies_per_error_rate))
errors = np.sqrt((np.square(energies - vqe_energy)))
standard_error = np.std(errors, axis=1)
error = np.sqrt((np.square(energies - vqe_energy)).mean(axis=1))
ax.errorbar(npzfile['error_level'], error, yerr=standard_error, fmt='None', capsize=0, capthick=2, color=UINT_COLOR,
            zorder=7)
ax.scatter(npzfile['error_level'], error, label=r'$U^{(int)}$ static', s=10, color=UINT_COLOR, zorder=8, marker='v')
params, cv = scipy.optimize.curve_fit(myExpFunc, np.log10(npzfile['error_level']), np.log10(error))
ax.plot(10 ** (x_values), 10 ** (myExpFunc(x_values, params[0], params[1])), linestyle='dotted', color=UINT_COLOR)

########################
### Make nice legend ###
########################
from matplotlib.lines import Line2D
from matplotlib.markers import MarkerStyle

marker = MarkerStyle(marker='v', joinstyle='round')

legend_elements = [
    Line2D([0], [0], color=LRX_COLOR, marker='o', linestyle='dashed', markersize=3, label=r'$LRX$ circuit to circuit'),
    Line2D([0], [0], color=LRZ_COLOR, marker='o', linestyle='dashed', markersize=3, label=r'$LRZ$ circuit to circuit'),
    Line2D([0], [0], color=UINT_COLOR, marker='o', linestyle='dashed', markersize=3, label=r'$U^{(int)}$ circuit to circuit'),
    Line2D([0], [0], color=LRX_COLOR, marker=marker, linestyle='dotted', markersize=3, label=r'$LRX$ static'),
    Line2D([0], [0], color=LRZ_COLOR, marker=marker, linestyle='dotted', markersize=3, label=r'$LRZ$ static'),
    Line2D([0], [0], color=UINT_COLOR, marker=marker, linestyle='dotted', markersize=3, label=r'$U^{(int)}$ static')
    ]
ax.legend(handles=legend_elements, loc='best')

###################
### Plot figure ###
###################
plt.tight_layout()
plt.savefig(f'{molecule}_error_model_plot.png', dpi=600)
plt.savefig(f'{molecule}_error_model_plot.pdf', dpi=600)
plt.show()
