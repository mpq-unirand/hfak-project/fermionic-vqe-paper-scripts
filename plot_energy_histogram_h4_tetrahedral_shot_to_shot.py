import numpy as np
from matplotlib import pyplot as plt


UINT_COLOR = '#f4ad4b'
LRX_COLOR = '#6dc4ba'
LRZ_COLOR = '#367479'

x_values = np.linspace(-3.2, -0.5,100)

plt.style.use('unirand')

SMALL_SIZE = 6
MEDIUM_SIZE = 8
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

fig = plt.figure(figsize=(3.3, 2.5))
ax = fig.add_subplot(111)
plt.xlabel('Energy deviation (Ha)')
plt.ylabel('Occurrence')


molecule = 'h4_tetrahedral'
number_of_energies_per_error_rate = 10
number_of_error_rates = 1

binwidth = 0.0001
y_min = 0
y_max = 35

plt.ylim(y_min, y_max)

x1, x2, y1, y2 = -0.00001, 0.00015, 0, 35

axins = ax.inset_axes(
    [0.07, 0.63, 0.35, 0.35],
    xlim=(x1, x2), ylim=(y1, y2))


npzfile = np.load(f'{molecule}.npz')
casci_energy = npzfile['casci_energy']
ax.vlines(casci_energy - casci_energy, y_min, y_max, color='black', linestyle='dashed', label='casci', zorder=11)
axins.vlines(casci_energy - casci_energy, y_min, y_max, color='black', linestyle='dashed', label='casci', zorder=11)
vqe_energy = npzfile['vqe_energy']
ax.vlines(vqe_energy - casci_energy, y_min, y_max, color='red', linestyle='dashed', label='vqe', zorder=12)
axins.vlines(vqe_energy - casci_energy, y_min, y_max, color='red', linestyle='dashed', label='vqe', zorder=12)

plotted_energy = 0

binwidth = 0.00001
outfile = f'{molecule}_LatticeRX_single_point.npz'
npzfile = np.load(outfile)
number_of_energies_per_error_rate = npzfile['number_of_repetitions_for_estimation']
number_of_error_rates = int(np.size(npzfile['all_energy_calculations']) / number_of_energies_per_error_rate)
energies = npzfile['all_energy_calculations'] - casci_energy
energies = np.reshape(energies, (number_of_error_rates, number_of_energies_per_error_rate))
ax.hist(energies[plotted_energy][:], label='RX', bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), alpha=0.7, color=LRX_COLOR, zorder=9)
ax.hist(energies[plotted_energy][:], bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), color=LRX_COLOR, histtype='step', zorder=10)
axins.hist(energies[plotted_energy][:], label='RX', bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), alpha=0.7, color=LRX_COLOR, zorder=9)
axins.hist(energies[plotted_energy][:], bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), color=LRX_COLOR, histtype='step', zorder=10)


binwidth = 0.0001
outfile = f'{molecule}_LatticeRZ_single_point.npz'
npzfile = np.load(outfile)
number_of_energies_per_error_rate = npzfile['number_of_repetitions_for_estimation']
number_of_error_rates = int(np.size(npzfile['all_energy_calculations']) / number_of_energies_per_error_rate)
energies = npzfile['all_energy_calculations'] - casci_energy
energies = np.reshape(energies, (number_of_error_rates, number_of_energies_per_error_rate))
ax.hist(energies[plotted_energy][:], label='RZ', bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), alpha=0.7, color=LRZ_COLOR, zorder=7)
ax.hist(energies[plotted_energy][:], bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), color=LRZ_COLOR, histtype='step', zorder=8)
axins.hist(energies[plotted_energy][:], label='RZ', bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), alpha=0.7, color=LRZ_COLOR, zorder=7)
axins.hist(energies[plotted_energy][:], bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), color=LRZ_COLOR, histtype='step', zorder=8)


binwidth = 0.0001
outfile = f'{molecule}_Uint_single_point.npz'
npzfile = np.load(outfile)
number_of_energies_per_error_rate = npzfile['number_of_repetitions_for_estimation']
number_of_error_rates = int(np.size(npzfile['all_energy_calculations']) / number_of_energies_per_error_rate)
energies = npzfile['all_energy_calculations'] - casci_energy
energies = np.reshape(energies, (number_of_error_rates, number_of_energies_per_error_rate))
ax.hist(energies[plotted_energy][:], label='Uint', bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), alpha=0.7, color=UINT_COLOR, zorder=5)
ax.hist(energies[plotted_energy][:], bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), color=UINT_COLOR, histtype='step', zorder=6)
axins.hist(energies[plotted_energy][:], label='Uint', bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), alpha=0.7, color=UINT_COLOR, zorder=5)
axins.hist(energies[plotted_energy][:], bins=np.arange(min(energies[plotted_energy][:]), max(energies[plotted_energy][:]) + binwidth, binwidth), color=UINT_COLOR, histtype='step', zorder=6)


ax.indicate_inset_zoom(axins, edgecolor="black")

from matplotlib.lines import Line2D
from matplotlib.patches import Patch
legend_elements = [Patch(facecolor=LRX_COLOR, edgecolor=LRX_COLOR, label=r'$LRX$'),
                   Patch(facecolor=LRZ_COLOR, edgecolor=LRZ_COLOR, label=r'$LRZ$'),
                   Patch(facecolor=UINT_COLOR, edgecolor=UINT_COLOR, label=r'$U^{(int)}$'),
                   Line2D([0], [0], color='black', linestyle='dashed', label='casci'),
                   Line2D([0], [0], color='red', linestyle='dashed', label='vqe')]
ax.legend(handles=legend_elements, loc='best')

plt.tight_layout()
plt.savefig(f'{molecule}_Histogram.pdf', dpi=600)
plt.show()