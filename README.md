# Scripts to generate plots for the fermionic VQE paper

This repository contains the scripts used to generate the plots for the paper "Simulating Chemistry with Fermionic Optical Superlattices".

For execution of the `lattice_decomposition_benchmark.py` script, the `pennylane_synqs` pennylane device is required. This can be found on the covestro gitlab.

## Contents

 - `data/`: Contains the raw data used to generate the plots.
   - `data/h4_tetrahedral.npz` contains the molecular data used to calculate the tetrahedral H4 molecule.
   - The remaining files contain the computed energies that are plotted in the final paper.
 - `lattice_decomposition_benchmark.py`: This script is used to lead the molecule data, decomposes the circuit to the lattice gates, appends the gate and estimates the energy. It outputs `.npz` files saving the computed energies and settings.
 - `error_model_plot.py`: This script is used to load the `.npz` files containing the computed energies and plot the error model plot in Figure 4.
 - `plot_energy_histogram_h4_tetrahedral_shot_to_shot.py`: This script is used to load the `.npz` files containing the computed energies and plot the energy histogram for the tetrahedral H4 molecule in Figure 7 a).
 - `plot_energy_histogram_h4_tetrahedral_systematic.py`: This script is used to load the `.npz` files containing the computed energies and plot the energy histogram for the tetrahedral H4 molecule in Figure 7 b).
  - `20250522_plot_h4_tetrahedral_circuit_data.py`: This script loads `.npy` file containing the fermionic Hamiltonian of the tetrahedral H4 molecule and plots the Hamiltonian matrix as well as the lowest eigenvector used for figure 6.

## Usage

 - Start by running the `lattice_decomposition_benchmark.py` script to generate the `.npz` files containing the computed energies. In the end of the scirpt you can define the parameters such as different error rates and the number of repetitions used.
 - Using these generated files you can call execute any of the plotting scripts (you may need to rename the file the script is loading).